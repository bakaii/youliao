﻿﻿<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>bill</title>
    <script type="text/javascript" src="/api/js/X-doc.js"></script>
</head>
<body style="height:100%; margin:0; overflow:hidden;">
<script id="myxdoc" type="text/xdoc" _format="pdf" style="width:100%;height:100%;">
    <xdoc version="A.3.0">
    <paper margin="0" width="300" height="550"/>
    <body padding="10" fillImg="#@f40">

        <para align="center" lineSpacing="15">
            <text fontName="行楷" fontSize="25" fontStyle="shadow">销售票据</text>
        </para>
        <para align="center" lineSpacing="5">
            <text fontColor="#ff0000" fontName="标宋" fontSize="12">XXXX公司</text>
        </para>
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="8">---------------------------------------------------------------------</text>
        </para>
        <para >
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="宋体" fontSize="13">1:物品</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13">萌贴心</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="宋体" fontSize="13">2:销售单号</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13"> XSDH123456</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="宋体" fontSize="13">3:经办人</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13">张三</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="宋体" fontSize="13">4:办理时间</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13" format="yyyy-MM-dd hh:mm:ss">2019-12-29 22:80:00</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="宋体" fontSize="13">5:交付金额</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13">$800.00</text>
                </para>
            </rect>
        </para>

        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="8">---------------------------------------------------------------------</text>
        </para>
        <para  align="center"  >
            <#--二维码-->
            <img   height="150" width="200" drawType="adjust"
                     src="http://localhost:8080/qrCode.png"/>
        </para>
        <para  align="center"  >
            <#--条形码-->
            <img   height="100" width="200" drawType="adjust"
                     src="http://localhost:8080/barCode.jpg"/>
        </para>
        </body>
    </xdoc>
</script>
</body>
</html>
